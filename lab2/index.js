"use strict";

const dayjs = require("dayjs");
const sqlite = require("sqlite3");
function Film(id, title, favorites = false, date = undefined, rating = undefined){
    this.id = id;
    this.title = title;
    this.favorites = favorites;
    this.date = date && dayjs(date)
    this.rating = rating;
}

function FilmLibrary(){
    const db = new sqlite.Database("films - copy.db", (err) => {
        if(err) 
            throw err
    });

    this.addNewFilm = (film) => {
        this.films.push(film);
    };

    this.sortByDate = () => {
        let results = [... this.films];
        results.sort((a, b) => {
            if(a.date == undefined)
                return 1;
            if(b.date == undefined)
                return -1;
            else
                return a.date.diff(b.date);
        });
        return results;
    }

    this.deleteFilm = (id) => {
        let index = undefined;
        for(let i=0; i < this.films.length; i++){
            if(this.films[i].id === id){
                index = i;
            }
        }
        console.log(index);
        if(index){
            let p1 = this.films.splice(0, index);
            let p2 = this.films.splice(index, this.films.length)
            this.films = [...p1, ...p2]; 
        }
    }

    this.resetWatchedFilms = () => {
        this.films.forEach((film) => film.date = undefined);
    }

    this.getRated = () => {
        console.log("***** Films filtered, only the rated ones *****");
        debugger;
        let filtered = this.films.filter((film) => film.rating)
        filtered.sort((a, b) => b.rating - a.rating).forEach((film) =>{
                console.log(`Id: ${film.id}, Title: ${film.title}, Favorite: ${film.favorites}, Watch date: ${film.date}, Score: ${film.rating}`)
            });
    };

    this.getAll = () => {
        return new Promise((resolve, reject) => {
            const sql = "SELECT * FROM FILMS";
            db.all(sql, (err, rows) => {
                if(err)
                    reject(err);
                else
                    resolve(rows.map((row) => new Film(row.id, row.title, row.favorite, row.watchdate, row.rating)));
            });
        })
    };

    this.getFavorite = () => {
        return new Promise((resolve, reject) => {
            const sql = "SELECT * FROM films WHERE favorite = 1";
            db.all(sql, (err, rows) =>{
                if(err)
                    reject(err);
                else
                    resolve(rows.map((row) => new Film(row.id, row.title, row.favorite, row.watchdate, row.rating)));
            })
        });
    };

    this.getWatchedToday = () => {
        return new Promise((resolve, reject) => {
            const sql = "SELECT * FROM films WHERE date IS NOT NULL";
            db.all(sql, (err, rows) =>{
                if(err)
                    reject(err);
                else
                    resolve(
                        rows.map((row) => new Film(row.id, row.title, row.favorite, row.watchdate, row.rating)).
                            filter((row) => row.date.isSame(dayjs()))
                    );
            })
        });
    }

    this.watchedBefore = (endDate) => {
        return new Promise((resolve, reject) => {
            const sql = "SELECT * FROM films WHERE watchdate < date(?)";
            db.all(sql, [endDate],(err, rows) =>{
                if(err)
                    reject(err);
                else
                    resolve(rows.map((row) => new Film(row.id, row.title, row.favorite, row.watchdate, row.rating)));
            })
        });
    };

    this.getHigherFilms = (minrating) => {
        return new Promise((resolve, reject) => {
            const sql = "SELECT * FROM films WHERE rating >= ?;";
            db.all(sql, [minrating], (err, rows) => {
                if(err)
                    reject(err);
                else
                    resolve(rows.map((row) => new Film(row.id, row.title, row.favorite, row.watchdate, row.rating)));
            });
        });
    };

    this.getFilmByTitle = (title) => {
        return new Promise((resolve, reject) => {
            const sql = "SELECT * FROM films WHERE title = ?;";
            db.all(sql, [title], (err, rows) => {
                if(err)
                    reject(err);
                else
                    resolve(rows.map((row) => new Film(row.id, row.title, row.favorite, row.watchdate, row.rating)));
            });
        }); 
    };

    this.store = (film) => {
        return new Promise((resolve, reject) => {
            const sql = "INSERT INTO FILMS(id, title, favorite, watchdate, rating) VALUES(?, ?, ?, ?, ?);"
            db.run(sql, [film.id, film.title, film.favorites, film.date, film.rating], (err) => {
                if(err)
                    reject(err);
                else    
                    resolve("Insert done");
            })
        });
    };

    this.deleteFromDB = (id) => {
        return new Promise((resolve, reject) => {
            const sql = "DELETE FROM FILMS WHERE id = ?";
            db.run(sql, [id], (err) => {
                if(err)
                    reject(err);
                else
                    resolve("Correctly deleted");
            });
        });
    };

    this.deleteWatchDate = () => {
        return new Promise((resolve, reject) => {
            const sql = "UPDATE films SET watchdate = null"
            db.run(sql, (err) => {
                if(err)
                    reject(err);
                else
                    resolve("Correctly updated");
            });
        });
    };
}

const library = new FilmLibrary();

library.getAll().then((films) => {
    console.log(films)
});

/*
library.getFavorite().then((films) => {
    console.log(films)
    //debugger;
});
*/

/* library.getWatchedToday().then((films) => {
    console.log(films)
    debugger;
}); */
/*
library.getHigherFilms(2).then((films) => {
    console.log(films)
});
*/
/*
library.getFilmByTitle("Star Wars").then((films) => {
    console.log(films)
});
*/
/*
library.store(new Film(9, "Spidernman: Far from home", false, null, null))
    .then((_) => {
        console.log(_);
    }).catch(
        (_) => {
            console.log(_);
        }
    )
    .then((_) => library.getAll()).then((films) =>{
        console.log(films);
    });

*/

/*
library.deleteFromDB(9)
    .then((_) => {
        console.log(_);
    })
    .catch((_) => {
        console.log(_);
    });
*/
/*
library.deleteWatchDate().
    then((_) => console.log(_))
    .catch((_) => console.log(_))
    .then((_) => library.getAll()).then((films) => {
        console.log(films);
        debugger;
    });
*/

library.getAll()
    .then((films) => console.log(films))
    .then((_) => library.watchedBefore("2022-03-20"))
    .then((films) => {
        console.log(films)
        debugger;
    });