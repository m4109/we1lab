"use strict";
const dayjs = require("dayjs")

function Film(id, title, favorites = false, date = undefined, rating = undefined){
    this.id = id;
    this.title = title;
    this.favorites = favorites;
    this.date = date && dayjs(date)
    this.rating = rating;
}

function FilmLibrary(){
    this.films = [];

    this.addNewFilm = (film) => {
        this.films.push(film);
    };

    this.sortByDate = () => {
        let results = [... this.films];
        results.sort((a, b) => {
            if(a.date == undefined)
                return 1;
            if(b.date == undefined)
                return -1;
            else
                return a.date.diff(b.date);
        });
        return results;
    }

    this.deleteFilm = (id) => {
        let index = undefined;
        for(let i=0; i < this.films.length; i++){
            if(this.films[i].id === id){
                index = i;
            }
        }
        console.log(index);
        if(index){
            let p1 = this.films.splice(0, index);
            let p2 = this.films.splice(index, this.films.length)
            this.films = [...p1, ...p2]; 
        }
    }

    this.resetWatchedFilms = () => {
        this.films.forEach((film) => film.date = undefined);
    }

    this.getRated = () => {
        console.log("***** Films filtered, only the rated ones *****");
        debugger;
        let filtered = this.films.filter((film) => film.rating)
        filtered.sort((a, b) => b.rating - a.rating).forEach((film) =>{
                console.log(`Id: ${film.id}, Title: ${film.title}, Favorite: ${film.favorites}, Watch date: ${film.date}, Score: ${film.rating}`)
            });
    };
}

const library = new FilmLibrary();

let f1 = new Film(1,"Pulp Fiction", true, "March 10, 2022", 5);
let f2 = new Film(2, "21 Grams", true, "March 17, 2022", 4);
let f3 = new Film(3, "Star Wars", false); 
let f4 = new Film(4, "Matrix", false);
let f5 = new Film(5, "Shrek", false, "March 21, 2022", 3);

library.addNewFilm(f1);
library.addNewFilm(f2);
library.addNewFilm(f3);
library.addNewFilm(f4);
library.addNewFilm(f5);

library.deleteFilm(2)
let sortedFilm = library.sortByDate()

//library.resetWatchedFilms();
library.getRated();
